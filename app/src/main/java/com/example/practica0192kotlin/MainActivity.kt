package com.example.practica0192kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var btnPulsar: Button;
    private lateinit var txtNombre: EditText;
    private lateinit var lblSaludar: TextView;
    private lateinit var btnLimpiar: Button;
    private lateinit var btnSalir: Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Relacionar los objetos
        btnPulsar = findViewById(R.id.btnSaludar)
        txtNombre = findViewById(R.id.txtNombre)
        lblSaludar = findViewById(R.id.lblSaludo)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnSalir = findViewById(R.id.btnSalir)

        // Codificar el boton para saludo
        btnPulsar.setOnClickListener {
            if(txtNombre.text.toString().isEmpty()) {
                Toast.makeText(this, "Faltó capturar información", Toast.LENGTH_SHORT).show()
            } else {
                val str = "Hola ${txtNombre.text.toString()} ¿cómo estás?"
                lblSaludar.text = str
            }
        }

        // Codificar el evento clic del boton Limpiar
        btnLimpiar.setOnClickListener {
            if(lblSaludar.text.toString() == ":: ::") {
                Toast.makeText(this, "La etiqueta está vacía", Toast.LENGTH_SHORT).show()
            } else {
                lblSaludar.text = ":: ::"
            }
        }

        // Codificar el evento clic del boton Salir
        btnSalir.setOnClickListener {
            finish()
        }
    }
}